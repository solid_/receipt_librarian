#!/bin/bash

# Create dir to accomodate receipts
mkdir receipts

# Install Redis and Python3 on Mac
brew install redis python3-dev
brew cask install DBngin

# Configure Redis for MACOS
brew services restart redis

# Install needed Libraries for python
pip3 install google-api-python-client redis nltk

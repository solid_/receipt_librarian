import sys

sys.path.append('/Users/solid/receipt_ocr/src/')

from tkinter import Tk, Label, Button, Entry, Text, DISABLED, END
import textindex
import subprocess
import os

class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        master.title("ReceiptFinder")

        self.label = Label(master, text="Jungle/Google OCR 4 Receipts")
        self.label.pack()
        
        self.textbox = Entry(master)
        self.textbox.pack()

        self.greet_button = Button(master, text="Search", command=self.search)
        self.greet_button.pack()
        
        self.greet_button = Button(master, text="Rescan", command=self.scan)
        self.greet_button.pack()
        
        self.output = Text(master, width=100, height=10)
        self.output.pack()

    def _process_query(self, query):
        return '\n'.join(list(map(lambda x: str(x[0]), query)))
        
    def search(self):
        self.output.delete(1.0, END)
        term = self.textbox.get()
        index = textindex.Index()
        result = index.print_lookup(term)
                
        if result:
            self.output.insert(END, self._process_query(result))
        else:
            self.output.insert(END, 'No matches found!')
            
    def scan(self):
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "/Users/solid/receipt_ocr/creds/creds.json"
        cmd = "python3 /Users/solid/receipt_ocr/src/textindex.py /Users/solid/receipt_ocr/receipts"
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()

# Receipt Librarian

The following package creates a central folder for all the receipts images. A small tool is provided to search text on these images.

The goal is to accelerate a co-worker job of reviewing receipts by enabling OCR and a text search engine on top of this image folder. Everything is kept at this Redis DB, avoiding repeate API calls, since they are paid.

The following package uses code made available by google @ https://github.com/GoogleCloudPlatform/cloud-vision/tree/master/python/text
